﻿using System;

namespace CqrsEventSourcingDotNet
{
	class Program
	{
		static void Main(string[] args)
		{
			var eb = new EventBroker();
			var p = new Person(eb);

			eb.Command(new ChangeNameCommand(p, "Federico"));
			eb.Command(new ChangeAgeCommand(p, 29));

			int age = eb.Query<int>(new AgeQuery { Target = p });
			string name = eb.Query<string>(new NameQuery { Target = p });

			Console.WriteLine($"{age}, {name}");

			eb.UndoLast();
			eb.UndoLast();

			age = eb.Query<int>(new AgeQuery { Target = p });
			name = eb.Query<string>(new NameQuery { Target = p });

			Console.WriteLine($"{age}, {name}");

			foreach (var e in eb.AllEvents)
			{
				Console.WriteLine(e);
			}

			Console.ReadKey();
		}
	}
}
