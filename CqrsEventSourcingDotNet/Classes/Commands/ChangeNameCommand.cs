﻿namespace CqrsEventSourcingDotNet
{
	class ChangeNameCommand : Command
	{
		public Person Target;
		public string Name;

		public ChangeNameCommand(Person target, string name)
		{
			Target = target;
			Name = name;
		}
	}
}
