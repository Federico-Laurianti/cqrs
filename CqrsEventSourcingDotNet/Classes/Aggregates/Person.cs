﻿namespace CqrsEventSourcingDotNet
{
	public class Person
	{
		private string name;
		private int age;

		EventBroker broker;

		public Person(EventBroker broker)
		{
			this.broker = broker;
			broker.Commands += BrokerOnCommands;
			broker.Queries += BrokerOnQueries;
		}

		private void BrokerOnQueries(object sender, Query query)
		{
			if (query is AgeQuery aq)
			{
				if (aq.Target == this)
				{
					aq.Result = age;
				}
			}
			else if (query is NameQuery nq)
			{
				if (nq.Target == this)
				{
					nq.Result = name;
				}
			}
		}

		private void BrokerOnCommands(object sender, Command command)
		{
			if (command is ChangeAgeCommand cac)
			{
				if (cac.Target == this)
				{
					if (cac.Register)
					{
						broker.AllEvents.Add(new AgeChangedEvent(this, age, cac.Age));
					}

					age = cac.Age;
				}
			}
			else if (command is ChangeNameCommand cnc)
			{
				if (cnc.Target == this)
				{
					if (cnc.Register)
					{
						broker.AllEvents.Add(new NameChangedEvent(this, name, cnc.Name));
					}

					name = cnc.Name;
				}
			}
		}
	}
}
